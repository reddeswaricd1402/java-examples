package com.classpath.functional;

public class IntegerTestClient {

	public static void main(String[] args) {
		IntegerMax obj = new IntegerMaxImpl();
		//int result = obj.max(91 ,76);
		//System.out.println("Result :: "+ result);
		
		//Anonymous class
		// step-1
		
		 IntegerMin minObj = new IntegerMin() {
			 @Override 
			 public int min(int a, int b) { 
				 if(a < b) { 
					 return a; 
				} else if( b < a){
					return b; 
				}
				 return -1;
				} 
		 };
		 

		// step-2
		 IntegerMin minObj2 = (int a, int b) -> { 
			 if (a < b) { 
				 return a; 
			 } else if( b < a) {
				 return b; 
			 } return -1; 
		 };

		// step-3
		 IntegerMin minObj3 = ( a, b) -> { 
			 if (a < b) { 
				 return a; 
			 } else if( b < a) {
				 return b; 
			 } return -1; 
		 };
		 

		//step-4
		IntegerMin minObj4 = (a, b) -> (a < b) ?  a : (b < a) ?  b : -1; 
		

		int result = minObj.min(45,  67);
		System.out.println("Result :: "+ result);
	}

}
