package com.classpath.functional;

public class IntegerMaxImpl implements IntegerMax{

	@Override
	public int max(int a, int b) {
		if(a > b) {
			return a;
		} else if( b > a) {
			return b;
		}
		return -1;
	}

}
