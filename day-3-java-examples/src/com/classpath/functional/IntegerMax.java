package com.classpath.functional;

public interface IntegerMax {

	public abstract int max(int a, int b); 
}
