package com.classpath.functional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.Set;
import java.util.function.IntPredicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamsDemo {
	
	public static void main(String[] args) {
		int[] values = {11,22,33,44,45,67};
		
		IntStream valueStream = Arrays.stream(values);
		
		List<String> list = List.of("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten");
		
		Set<String> setStream = Set.of("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten");
		
		
		Supplier<Integer> intSupplier = () -> (int)Math.random()*100000;
		Stream<String> supplierStream = Stream.generate(()-> "hello world");
		
		IntStream intStream = Arrays.stream(new int[]{11,11, 21, 14, 67,22,33,21,14,67,45,34});
		
		//calculate the sum of the values
		
		OptionalInt totalSum = valueStream.reduce((operand1, operand2) -> operand1 + operand2);
		//System.out.println("Sum of all the numbers :: "+totalSum.getAsInt());
		
		//OptionalInt minIteger = intStream.reduce((i1, i2) -> Integer.min(i1, i2));
		
		//System.out.println("Min value from the stream is :: "+minIteger.getAsInt());
		
		// process all the ages which are less than 18 
		
		long count = intStream
						.distinct()
						.filter(age -> age < 18)
						.count();
		
		//primitive values
		IntPredicate result = value -> value > 26;
		
		List<String> output = list.stream()
							      .filter(value -> value.equalsIgnoreCase("six") || value.equalsIgnoreCase("seven") || value.equalsIgnoreCase("ten"))
							      .collect(Collectors.toList());
		
		
		boolean ifTenExists = list.stream().anyMatch(value -> value.equalsIgnoreCase("ten"));
							
					output.forEach(value -> System.out.println(value));
					
					
		Optional<String> optioanlResult = list
											.stream()
												.filter(item -> item.equalsIgnoreCase("ten"))
												.findAny();
		if(optioanlResult.isPresent()) {
			System.out.println("INside the optional check");
			String resultValue = optioanlResult.get();
			System.out.println("Result value :: "+ resultValue);
		}else {
			System.out.println("The result is empty");
		}
					
		//System.out.println("Total number less than 18 is "+ count);
	}

}
