package com.classpath.functional;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class StreamsTest {

	public static void main(String[] args) {
		User ramesh = new User(12, "Ramesh");

		User suresh = new User(14, "Suresh");

		User vinay = new User(15, "Vinay");
		
		User vikram = new User(21, "Vikram");

		User hari = new User(24, "Hari");

		List<User> users = new ArrayList<>();

		users.add(ramesh);
		users.add(suresh);
		users.add(hari);
		users.add(vinay);
		users.add(vikram);
		
		// Predicate, Function, Consumer
		
		// Filter out all the users between 20 and 22 years of age and print their names
		Predicate<User> userAgeBetween20And22 = user -> user.getAge() > 20 && user.getAge() < 22;
		users
			.stream()
			.filter(userAgeBetween20And22)
			.map(User::getName)
			.forEach(System.out:: println);
		
		
		// Print all the users age in days (age * 365)
		System.out.println("***************************");
		users
		.stream()
		.map(User::getAge)
		//syntactical sugar
		.map(age -> age* 365)
		.forEach(System.out::println);
		
		// Print all the users name in the ascending order of their age
		System.out.println("***************************");
		
		users
		.stream()
		.sorted((user1, user2) -> Integer.compare(user1.getAge(), user2.getAge()))
		.map(user -> "Name :: "+ user.getName() + "," +" Age: "+user.getAge())
		.forEach(ageInDays -> System.out.println(ageInDays));

		
		// Print all the names of the users in the capital letters
		System.out.println("***************************");
		users
		.stream()
		.map(User:: getName)
		.map(String:: toUpperCase)
		.forEach(System.out:: println);
		

	}

}
