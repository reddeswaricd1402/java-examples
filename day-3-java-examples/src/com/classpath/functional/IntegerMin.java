package com.classpath.functional;

@FunctionalInterface
public interface IntegerMin {

	int min(int a, int b); 
}
