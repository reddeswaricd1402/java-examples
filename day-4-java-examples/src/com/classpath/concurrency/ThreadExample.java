package com.classpath.concurrency;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class ThreadExample {
	
	public static void main(String[] args) throws InterruptedException {
		//currenty the code is running on the main thread.
		
		System.out.println(" Number of cores on the system :: "+ Runtime.getRuntime().availableProcessors());
		//creating a new thread creates a java object
		Task childThread1 = new Task("child-thread-1");
		Task childThread2 = new Task("child-thread-2");
		Task childThread3 = new Task("child-thread-3");
		Task childThread4 = new Task("child-thread-4");
		
		childThread1.start();
		childThread2.start();
		childThread3.start();
		childThread4.start();
		
		Thread.sleep(Duration.of(4, ChronoUnit.SECONDS).toMillis());
		
		
		//blocked till all the three thread completes
		childThread1.join();
		childThread2.join();
		childThread3.join();
		childThread4.join();
		
		System.out.println("The main thread executing is compelete::");
	}

}

class Task extends Thread {
	
	public Task(String name) {
		super(name);
	}
	
	@Override
	public void run() {
		System.out.println("Running the job in a separate thread:: ");
		System.out.println("Name of the thread executing this method :: "+ Thread.currentThread().getName());
		try {
			Thread.sleep(Duration.of(8, ChronoUnit.SECONDS).toMillis());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("The child job is completed::::");
		
	}
	
}
