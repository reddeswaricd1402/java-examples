package com.classpath.concurrency;

import static java.lang.Thread.sleep;
import static java.time.Duration.of;
import static java.time.temporal.ChronoUnit.SECONDS;
import java.time.LocalDateTime;

public class ThreadDemo {
	
	public static void main(String[] args) {
		String name = Thread.currentThread().getName();
		System.out.println("Name of the current thread :: "+ name);
		System.out.println("System time :: "+ LocalDateTime.now());
		try {
			sleep(of(5, SECONDS).toMillis());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("System time :: "+ LocalDateTime.now());
	}

}
