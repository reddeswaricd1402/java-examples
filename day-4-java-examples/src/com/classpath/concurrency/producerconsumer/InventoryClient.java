package com.classpath.concurrency.producerconsumer;

public class InventoryClient {
	
	public static void main(String[] args) throws InterruptedException {
		Inventory inventory = new Inventory(10);
		
		Producer producer = new Producer(inventory);
		Consumer consumer = new Consumer(inventory);
		
		Thread producerThread = new Thread(producer);
		Thread consumerThread = new Thread(consumer);
		
		producerThread.start();
		consumerThread.start();
		
		
		producerThread.join();
		consumerThread.join();
		
		
	}

}
