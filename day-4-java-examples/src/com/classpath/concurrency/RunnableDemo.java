package com.classpath.concurrency;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class RunnableDemo {
	
	public static void main(String[] args) throws InterruptedException {
		//currenty the code is running on the main thread.
		
		System.out.println(" Number of cores on the system :: "+ Runtime.getRuntime().availableProcessors());
		//creating a new thread creates a java object
		Job task = new Job();
		
		Thread thread1 = new Thread(task, "thread-1");
		Thread thread2 = new Thread(task, "thread-2");
		Thread thread3 = new Thread(task,"thread-3");
		Thread thread4 = new Thread(task, "thread-4");
		
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		
		
		//blocked till all the three thread completes
		thread1.join();
		thread2.join();
		thread3.join();
		thread4.join();
		
		System.out.println("The main thread executing is compelete::");
	}

}

class Job implements Runnable {
	
	@Override
	public void run() {
		try {
			Thread.sleep(Duration.of(8, ChronoUnit.SECONDS).toMillis());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("The child job is completed::::" + Thread.currentThread().getName());
	}
}
