package com.classpath.concurrency;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class Printer {
	
	private int numberOfPages;
	
	public Printer(int numberofPages) {
		this.numberOfPages = numberofPages;
	}


	public synchronized void print() {
		for(int i = numberOfPages; i >= 0 ; i --) {
			try {
				Thread.sleep(Duration.of(2, ChronoUnit.SECONDS).toMillis());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Printing the page number :: "+ i + " for the User :: "+ Thread.currentThread().getName());
		}
	}
}

class PrinterTask implements Runnable{
	
	private Printer printer;
	
	public PrinterTask(Printer printer) {
		this.printer = printer;
	}

	@Override
	public void run() {
		this.printer.print();
	}
	
}
