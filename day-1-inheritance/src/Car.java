
public class Car {
	
	//attributes 
	/*
	 * speed - 67.09
	 * vehicleNumber - "KA-05-MB2345"
	 * make - BMW, Audi, Suzuki
	 * color
	 */
	
	// constructor (vehicle nu, make, color
	
	//methods, accelerate, slowDown, stop
	private double speed;
	private String vehicleNumber;
	private String make;
	private String color;
	
	private static int counter;
	
	public Car(String vehicleNumber, String make, String color) {
		this.vehicleNumber = vehicleNumber;
		this.make = make;
		this.color = color;
		counter++;
	}
	
	public void accelerate() {
		this.speed++;
	}
	
	public void slowDown() {
		this.speed--;
	}
	
	public void stop() {
		this.speed = 0;
	}
	
	public double getSpeed() {
		return this.speed;
	}
	
	public static int getTotalNumberOfCars() {
		return counter;
	}
	
	public static void main(String[] args) {
		Car car1 = new Car("KA-05 MB-3456", "TATA_SUMO", "white");
		Car car2 = new Car("KA-05 MB-3456", "TATA_SUMO", "white");
		Car car3 = new Car("KA-05 MB-3456", "TATA_SUMO", "white");
		Car car4 = new Car("KA-05 MB-3456", "TATA_SUMO", "white");
		Car car5 = new Car("KA-05 MB-3456", "TATA_SUMO", "white");
		Car car6 = new Car("KA-05 MB-3456", "TATA_SUMO", "white");

		
		car1.accelerate();
		car1.accelerate();
		car1.accelerate();
		car1.accelerate();
		
		System.out.println("The speed after acclerating is : "+ car1.getSpeed());
		
		car1.slowDown();
		car1.slowDown();
		
		System.out.println("The speed after slowdown is : "+ car1.getSpeed());
		
		car1.stop();
		
		System.out.println("The speed after stopping is : "+ car1.getSpeed());
		
		System.out.println("Total number of cars sold is "+ Car.getTotalNumberOfCars());
	}

}
