
public class Dentist extends Doctor {
	
	
	public Dentist (String name, int age) {
		super(name, age);
	}
	
	public void treatKids(String patientName) {
		System.out.println("Treating kids .... " + patientName);
	}
	
	
	@Override
	public void treatPatient(String patientName) {
		treatKids(patientName);
	}
	
}
