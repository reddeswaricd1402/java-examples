
public class OrthoPedician extends Doctor {
	
	public OrthoPedician (String name, int age) {
		super(name, age);
	}
	
	public void conductXRay(String patientName) {
		System.out.println("Conducting X-Ray for "+ patientName);
	}
	
	public void conductCTScan(String patientName) {
		System.out.println("Conducting CT-Scan for "+patientName);
	}
	
	@Override
	public void treatPatient(String patientName) {
		conductXRay( patientName);
		conductCTScan(patientName);
	}

}
