
//inheritance
public abstract class Doctor {
	
	private String name;
	private int age;
	
	public Doctor(String name, int age) {
		this.name = name;
		this.age = age;
	}

	//constructor overloading
	public Doctor(String name) {
		this.name = name;
	}
	
	public abstract void treatPatient(String patientName);
	
	public final double consultationFee() {
		return 300D;
	}

}
