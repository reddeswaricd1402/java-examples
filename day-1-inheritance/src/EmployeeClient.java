
public class EmployeeClient {
	
	public static void main(String[] args) {
		PermanentEmployee permanentEmployee = new PermanentEmployee("Rahul");
		ContractEmployee contractEmployee = new ContractEmployee("Kiran");
		
		permanentEmployee.applyForLeave(10);
		
		contractEmployee.applyForLeave(10);
		
		
		double salaryForPermanentEmployee = permanentEmployee.paySalary();
		double salaryForContractEmployee = contractEmployee.paySalary();
		
		
		System.out.println("Employee id of Permanenet employee :: "+ permanentEmployee.getEmpId());
		System.out.println("Employee id of Contract employee :: "+ contractEmployee.getEmpId());
		
		System.out.println("Salary for permanent Employee is "+ salaryForPermanentEmployee);
		System.out.println("Salary for contract Employee is "+ salaryForContractEmployee);

		
		System.out.println("Available leaves for permanent Employee is "+ permanentEmployee.getAvailableLeaves());
		System.out.println("Contract duration for contract Employee is "+ contractEmployee.getContractDuration());

	}

}
