package com.classpath.emp.service;

import java.util.Set;

import com.classpath.emp.dao.EmployeeRepository;
import com.classpath.emp.model.Employee;


//TODO - 7
// add the methods to save dependents and find all the dependents with empid
public class EmployeeService {
	
	private final EmployeeRepository employeeRepository;
	
	//constructor based injection
	public EmployeeService(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	
	//TODO-3
	/*
	 * constructor to initialize the employeeRepository
	 * write all the method for save, create, delete and find
	 * Delegate the logic to employeeRepository  
	 */
	
	public Employee saveEmployee(Employee employee) {
		return this.employeeRepository.addEmployee(employee);
	}
	
	public Employee findEmployeeById(int id) {
		return this.employeeRepository
						.findEmployeeById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid employee id"));	
								
	}
	
	public Set<Employee> fetchAllEmployees(){
		return this.employeeRepository.fetchAllEmployees();
	}
	
	public void deleteEmployeeById(int empId) {
		this.employeeRepository.deleteEmployeeById(empId);
	}

}
