package com.classpath.emp.model;

import java.util.Objects;

/*
 * class 
 * POJO - Plain old Java object
 * DTO - Domain transfer object
 * Entity 
 * model
 */
public class Employee {
	
	//TODO -1
	//TODO - 5
	 /*
	  * Add Dependents to employees
	  * Dependent will have (Set)
	  *    - name
	  *    - age
	  *    - relationship (Spouse, Father, Mother, Child)
	  * One employee can have more than one dependent 
	  * Every employee will have an address
	  * 
	  * Address will have 
	  *  - street
	  *  - city
	  *  - zip
	  *  - state
	  */
		/*
		 * Constructor
		 * Setters and Getters
		 * toString
		 * equals
		 * hashcode
		 */
	private int empId;
	private String name;
	private double salary;
	private static int idCounter = 100;
	
	public Employee(String name, double salary) {
		this.name = name;
		this.salary = salary;
		this.empId  = idCounter++;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(empId, name, salary);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		return empId == other.empId && Objects.equals(name, other.name)
				&& Double.doubleToLongBits(salary) == Double.doubleToLongBits(other.salary);
	}
	
	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", name=" + name + ", salary=" + salary + "]";
	}

}
