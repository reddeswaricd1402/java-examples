package com.classpath.exceptions;

public class ProgramWithException {

	/*
	 * Exceptions in Java can be classified into two types - Checked exception -
	 * Unchecked/Runtime exception
	 * 
	 * CheckedException - Which extends directy or indirectly Exception class is
	 * called CheckedException - Compiler will force you to handle checked exception
	 * - ex: - IOException - FileNotFoundException
	 * 
	 * 
	 * Unchecked Exception - Which directly or indirectly extends RuntimeException
	 * is called Unchecked/RuntimeException - Compiler will not force you to handle
	 * UncheckedException - ex: - NullPointerException - ArithmeticExceptin -
	 * ArrayIndexOutOfBoundsException
	 */
	public static void main(String[] args) {

		int operand1 = 90;
		int operand2 = 100;

		int[] values = { 10, 34, 2, 12 };

		try {

			if (operand2 > operand1) {
				throw new IllegalArgumentException(" Invalid operand");
			}
			System.out.println("Result of division :: " + (operand1 / operand2));
			System.out.println("Accesing the element of the array :: " + values[0]);
			/*
			 * } catch (ArithmeticException exception) {
			 * System.out.println("Exception while dividing a number by zero."); }
			 * catch(ArrayIndexOutOfBoundsException exception) {
			 * System.out.println("Accesssing invalid index from an array"); }
			 */
		} catch (ArithmeticException | ArrayIndexOutOfBoundsException exception) {
			System.out.println("invalid input");
		} catch (IllegalArgumentException e) {
			System.out.println("Invalid operand 2");
		} catch (Exception exceptin) {
			System.out.println("Generic Exception. something went wrong !!");
		} finally {
			// this is done for cleaning up the resources..
			System.out.println("This statement will get executed in all the conditions ..");
		}

	}

}
