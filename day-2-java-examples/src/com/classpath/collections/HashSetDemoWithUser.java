package com.classpath.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

public class HashSetDemoWithUser {
	
	public static void main(String[] args) {
		
		//1. create 10 different users
		Set<User> users = new HashSet<>();
		
		User user1 = new User(1, "Ramesh", 26);
		User user2 = new User(2, "Harish", 22);
		User user3 = new User(3, "Suresh", 21);
		User user4 = new User(4, "Bipin", 45);
		User user5 = new User(5, "Vinay", 56);
		User user6 = new User(6, "Vikrant", 12);
		User user7= new User(7, "Usha", 57);
		User user8 = new User(8, "Ajay", 18);
		User user9 = new User(9, "Babu", 32);
		User user10 = new User(10, "Somesh", 28);
		
		//2. Add them to the Hashset
		users.add(user1);
		users.add(user1);
		users.add(user1);
		users.add(user1);
		users.add(user2);
		users.add(user3);
		users.add(user4);
		users.add(user5);
		users.add(user6);
		users.add(user7);
		users.add(user8);
		users.add(user9);
		users.add(user10);
		
		System.out.println("Total size of the users :: "+ users.size());
		
		//3. Iterate over the HashSet and print the User
		Iterator<User> it = users.iterator();
		while(it.hasNext()) {
			User user = it.next();
			System.out.println(user);
		}
		
		//4. Delete all the elements of the Hashset
		users.clear();
		
		 //5. Print if the hashSet is empty.
		System.out.println("Is the set empty ?? "+ users.isEmpty());
		
		
	}

}

class User {
	
	private int id;
	private String name;
	private int age;
	
	
	public User(int id, String name, int age) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", age=" + age + "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(age, id, name);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return age == other.age && id == other.id && Objects.equals(name, other.name);
	}
	
	
	
	
	
	
}
