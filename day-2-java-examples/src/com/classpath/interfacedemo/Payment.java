package com.classpath.interfacedemo;

public interface Payment {
	
	boolean acceptPayment(String merchant, String customer, double amount, String notes);

}