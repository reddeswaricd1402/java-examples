package com.classpath.interfacedemo;

public class PhonePay implements Payment, PhoneRecharge{

	@Override
	public boolean acceptPayment(String merchant, String customer, double amount, String notes) {
		System.out.println("Making a payment of Rs "+ amount + " from "+ merchant+ " to "+ customer + " notes: "+notes + " using Phone Pay");
		System.out.println(" Redeem points :: "+ (2 * amount));
		return true;
	}


	@Override
	public void recharge(String phoneNumber, double amount) {
		System.out.println("Recharge done for "+ phoneNumber +" amount "+ amount);
		System.out.println("Recharge done with Phone Pay");
		
	}

}
