package com.classpath.interfacedemo.client;

import java.util.Scanner;
import com.classpath.interfacedemo.AmazonPay;
import com.classpath.interfacedemo.GooglePay;
import com.classpath.interfacedemo.Payment;
import com.classpath.interfacedemo.PhonePay;

public class InterfaceClient {
	
	public static void main(String[] args) {
		
		Payment payment = null;
		
		Scanner scanner  = new Scanner(System.in);
		System.out.println("Please enter your option ");

		System.out.println("1: Google Pay ");
		System.out.println("2: Phone Pay");
		System.out.println("3: Amazon Pay");
		
		int option = scanner.nextInt();
		
		switch(option) {
		case 1: 
			payment = new GooglePay();
			break;
		case 2:
			payment = new PhonePay();
			break;
		case 3:
			payment = new AmazonPay();
			break;
		default:
			payment = new GooglePay();
		}
		
		payment.acceptPayment("Big-Bazaar", "Naveen", 1500, "Bill payment for Groceries");
		
		scanner.close();
		
	}

}

