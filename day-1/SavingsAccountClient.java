public class SavingsAccountClient {

    public static void main(String[] args) {
        SavingsAccount account = new SavingsAccount("Vinod", 50000);
        account.deposit(25_000);
        System.out.println("Initial account balance:: "+ account.getCurrentBalance());

        account.withdraw(10_000);
        System.out.println("Account balance after withdraw:: "+ account.getCurrentBalance());

        account.withdraw(10_000);
        System.out.println("Account balance after withdrawing for second time:: "+ account.getCurrentBalance());

        
        account.withdraw(10_000);
        System.out.println("Account balance after withdrawing for second time:: "+ account.getCurrentBalance());
    }
    
}
