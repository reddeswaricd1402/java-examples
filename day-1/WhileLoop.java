
public class WhileLoop {
    public static void main(String[] args) {
        
        //for loop
        int[] ages = new int[10];

        //assinging the values
        ages[0] = 12;
        ages[1] = 22;
        ages[2] = 34;
        ages[3] = 45;
        ages[4] = 17;
        ages[5] = 26;
        ages[6] = 14;
        ages[7] = 36;
        ages[8] = 15;
        ages[9] = 23;

        boolean flag = true;
        while(flag){
            int counter = 0;
            if(counter >= ages.length) {
                flag = false;
            }
            System.out.println("Value of age is "+ ages[counter++]);
        }
    }
   
}
